from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computadora import Computadora as ComputadorasModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer

computadora_router = APIRouter()

class Computadoras(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    marca: str = Field(default="Marca computadora", min_length=2, max_length=20)
    modelo: str = Field(default="Modelo de la computadora", min_length=2, max_length=50)
    color: str = Field(default="Color de la computadora", min_length=2, max_length=50)
    ram: int = Field(default="RAM computadora")
    almacenamiento: str = Field(default="Almacenamiento computadora",min_length=2, max_length=20)

    class Config:
        json_schema_extra ={
            "example":{
                "id": 1,
                "marca": "Marca Computadora",
                "modelo": "Modelo Computadora",
                "color": "Color Computadora",
                "ram": 16,
                "almacenamiento": "256 GB SSD"
            }
        }

@computadora_router.get('/computadoras', tags=['computadoras'], response_model=List[Computadoras])
def get_computadoras() -> List[Computadoras]:
    db = Session()
    result2 = db.query(ComputadorasModel).all()
    return JSONResponse(status_code=200, content= jsonable_encoder(result2))

@computadora_router.get('/computadoras/{id}', tags=['computadoras'])
def get_computadora_by_id(id: int = Path(ge=1, le=2000)) -> Computadoras:
    db = Session()
    result2 = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not result2:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result2))

   
@computadora_router.get('/computadoras/', tags=['computadoras'])
def get_computadoras_by_marca(marca: str = Query(min_length=5, max_length=15)) -> List[Computadoras]:
    db = Session()
    result2 = db.query(ComputadorasModel).filter(ComputadorasModel.marca == marca).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result2))


@computadora_router.post('/computadoras/', tags=['computadoras'], response_model= List[Computadoras], status_code=200)
def post_computadora(computadora: Computadoras) -> dict:
    db = Session()
    new_computadora = ComputadorasModel(**computadora.model_dump())
    db.add(new_computadora)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})

@computadora_router.put('/computadoras/{id}', tags=['computadoras'], response_model= List[Computadoras], status_code=200)
def update_computadora(id: int, computadora: Computadoras) -> dict:
    db = Session()
    result2 = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not result2:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    result2.marca = computadora.marca
    result2.modelo = computadora.modelo
    result2.color = computadora.color
    result2.ram = computadora.ram
    result2.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la computadora"})
    
@computadora_router.delete('/computadoras/{id}', tags=['computadoras'], response_model= List[Computadoras], status_code=200)
def delete_computadora(id: int) -> dict:
    db = Session()
    result2 = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not result2:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    db.delete(result2)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})


