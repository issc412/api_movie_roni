from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from utils.jwt_manager import create_token
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routes.movie import movie_router
from routes.computadora import computadora_router
from routes.user import user_router

app = FastAPI()
app.title = "Computadoras y Películas"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(computadora_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>BIENVENIDOS A MI TIENDA DE COMPUTADORAS</h1>')

